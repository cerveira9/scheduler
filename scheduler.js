require('dotenv').config({ path: './config/.env' });
var batida = require('./utils/constantsUtils');

var cron = require('node-cron');
var log4js = require("log4js");
var logger = log4js.getLogger();

const puppeteer = require('puppeteer');
const dateTimeUtils = require('./utils/dateTimeUtils');
const randomHourUtils = require('./utils/sleepTimeUtils');

logger.level = "info";

cron.schedule(batida.PONTO_INICIO_EXPEDIENTE, () => {
    logger.info('Iniciado processo de bater PONTO_INICIO_EXPEDIENTE as ' + dateTimeUtils.getDateTime());
    getReportTime();
}, {
    scheduled: true,
    timezone: "America/Sao_Paulo"
});

cron.schedule(batida.PONTO_SAIDA_ALMOCO, () => {
    logger.info('Iniciado processo de bater PONTO_SAIDA_ALMOCO as ' + dateTimeUtils.getDateTime());
    getReportTime();
}, {
    scheduled: true,
    timezone: "America/Sao_Paulo"
});

cron.schedule(batida.PONTO_RETORNO_ALMOCO, () => {
    logger.info('Iniciado processo de bater PONTO_RETORNO_ALMOCO as ' + dateTimeUtils.getDateTime());
    getReportTime();
}, {
    scheduled: true,
    timezone: "America/Sao_Paulo"
});

cron.schedule(batida.PONTO_FINAL_EXPEDIENTE, () => {
    logger.info('Iniciado processo de bater PONTO_FINAL_EXPEDIENTE as ' + dateTimeUtils.getDateTime());
    getReportTime();
}, {
    scheduled: true,
    timezone: "America/Sao_Paulo"
});

function getReportTime() {
    let reportSleepTime = randomHourUtils.getReportSleepTime() * 1000;
    logger.info('Tempo de espera: ' + reportSleepTime / 1000 + ' segundos.');

    setTimeout(() => {
        registerHour();
      }, reportSleepTime)
}

function registerHour() {

    logger.info('PONTO_BATIDO as ' + dateTimeUtils.getDateTime());

    (async () => {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.goto('https://app.pmovel.com.br/');
        await page.waitForSelector('#email');
        await page.type('#email', process.env.USER_EMAIL);
        await page.waitForSelector('#password');
        await page.type('#password', process.env.USER_PASSWORD);
        await page.waitForSelector('.btn-primary');
        await page.click('.btn-primary');
        // Descomente essa parte para efetuar a batida
        // await page.waitForSelector('.btn-success');
        // await page.click('.btn-success');

        await browser.close();
    })();
}